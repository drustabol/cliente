package gui;

import secure.Crypto;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * Clase que muestra la interfaz de logeado o ingreso en la aplicación. Dependiendo del tipo de usuario, se enviará a
 * una interfaz u otra, que es de carácter administrador o bien usuario
 */
public class Login extends JFrame {
    private VistaUsuario vistaUsuario;
    private VistaAdmin vistaAdmin;
    private Socket socket;
    private int puerto;
    private String host;
    private DataInputStream entrada;
    private DataOutputStream salida;

    private JPanel panelLogin;
    private JTextField campoUsuario;
    private JPasswordField campoPassword;
    private JLabel etiquetaUsuario, etiquetaPassword, welcome, logo;
    private JButton acceder;
    byte contador = 0;
    boolean acceso = false;
    private String clave="claveChunguisima";

    /**
     * Constructor por el que se inicializa la ventana de login
     */

    public Login() {
        this.setTitle("Ingreso");
        this.setSize(1024, 768);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        inicioPanel();

        inicioEtiquetas();
        inicioUsuario();
        comprobarCredenciales();
        this.puerto=4444;
        this.host="localhost";

    }

    /**
     * Método por el que se dan los atributos al panel de la ventana
     */

    public void inicioPanel() {
        panelLogin = new JPanel();
        panelLogin.setLayout(null);
        panelLogin.setBackground(new Color(254, 242, 113));
        this.setVisible(true);
        this.getContentPane().add(panelLogin);

    }

    /**
     * Método por el que se sitúan aquellas partes de la ventana que no requieren la interactuación del usuario
     */

    private void inicioEtiquetas() {
        welcome = new JLabel("Bienvenido. Por favor, introduce tu usuario y contraseña");

        logo = new JLabel();

        logo.setBounds(500, 100, 600, 600);
        logo.setIcon(new ImageIcon("logo.png"));

        panelLogin.add(logo);

        etiquetaUsuario = new JLabel("Usuario");
        etiquetaPassword = new JLabel("Password");

        welcome.setBounds(120, 170, 800, 25);
        welcome.setFont(new Font("Sans Serif", Font.BOLD, 24));
        panelLogin.add(welcome);

        etiquetaUsuario.setBounds(70, 280, 150, 25);
        etiquetaUsuario.setFont(new Font("Sans Serif", Font.BOLD, 18));
        panelLogin.add(etiquetaUsuario);

        etiquetaPassword.setBounds(70, 320, 150, 25);
        etiquetaPassword.setFont(new Font("Sans Serif", Font.BOLD, 18));
        panelLogin.add(etiquetaPassword);

    }

    /**
     * Método que fija los campos de login y password que requieren la intervención del usuario
     */

    private void inicioUsuario() {
        campoUsuario = new JTextField();
        campoPassword = new JPasswordField();

        campoUsuario.setBackground(Color.WHITE);
        campoUsuario.setBounds(170, 280, 150, 25);
        campoUsuario.setFont(new Font("Sans Serif", Font.BOLD, 20));
        panelLogin.add(campoUsuario);

        campoPassword.setBackground(Color.WHITE);
        campoPassword.setBounds(170, 320, 150, 25);
        campoPassword.setFont(new Font("Sans Serif", Font.BOLD, 20));
        panelLogin.add(campoPassword);

    }

    /**
     * Método por el que la aplicación se conecta con el servidor y envía los datos introducidos por el usuario
     * encriptados, y luego recibe del mismo modo encriptados, pero son desencriptados en la la clase Crypto
     */

    private void comprobarCredenciales() {
        acceder = new JButton("Entrar");
        acceder.setBounds(220, 370, 120, 25);
        acceder.setFont(new Font("Sans Serif", Font.BOLD, 18));
        acceder.setBackground(new Color(190, 177, 35));
        panelLogin.add(acceder);
        repaint();

        acceder.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String s = "";
                String user=campoUsuario.getText();

                for (int i = 0; i < campoPassword.getPassword().length; i++) s += campoPassword.getPassword()[i];

                try {
                    Crypto crypto= new Crypto();
                    socket=new Socket (host,puerto);

                    salida=new DataOutputStream(socket.getOutputStream());
                    entrada=new DataInputStream(socket.getInputStream());

                    salida.writeUTF(crypto.encriptacion(clave,user));
                    salida.writeUTF(crypto.encriptacion(clave,s));

                    try {
                        String recibida = crypto.desencriptado(clave, entrada.readUTF());

                        if (recibida.equalsIgnoreCase("true")){
                            recibida = crypto.desencriptado(clave, entrada.readUTF());
                            if (recibida.equalsIgnoreCase("usuario")){
                                acceso=true;
                                vistaUsuario= new VistaUsuario();
                                vistaUsuario.setVisible(true);
                                dispose();
                            }
                            else if (recibida.equalsIgnoreCase("root")) {
                                acceso=true;
                                vistaAdmin= new VistaAdmin();
                                vistaAdmin.setVisible(true);
                                dispose();
                            }

                        }else acceso=false;
                    }catch (EOFException e1) {
                        acceso=false;
                    }

                    salida.close();
                    entrada.close();
                    socket.close();

                } catch (NoSuchAlgorithmException ex) {
                    ex.printStackTrace();
                } catch (NoSuchPaddingException | IOException ex) {
                    ex.printStackTrace();
                } catch (IllegalBlockSizeException illegalBlockSizeException) {
                    illegalBlockSizeException.printStackTrace();
                } catch (BadPaddingException badPaddingException) {
                    badPaddingException.printStackTrace();
                } catch (InvalidKeyException invalidKeyException) {
                    invalidKeyException.printStackTrace();
                }

                if (!acceso) {
                    contador++;
                    JOptionPane.showMessageDialog(null, "Intento " + contador + " Fallo",
                            "Usuario y contraseña incorrectos", +JOptionPane.WARNING_MESSAGE);
                    if (contador == 3) {
                        JOptionPane.showMessageDialog(null,
                                "Has sobrepasado el número de intentos. Inténtalo más tarde",
                                "Número de intentos sobrepasado", +JOptionPane.WARNING_MESSAGE);
                        System.exit(0);
                    }
                }
                campoUsuario.setText("");
                campoPassword.setText("");
            }
        });
    }
}