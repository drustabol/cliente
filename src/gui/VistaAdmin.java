package gui;

import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;

public class VistaAdmin extends JFrame {

    //Pestaña Chóferes
    private JPanel panelChoferes;
    JLabel lblNombreChofer;
    JLabel lblApellidosChofer;
    JLabel lblFechaNac;
    JLabel lblFechaInicio;
    JLabel lblFechaFin;
    JLabel lblNSegSoc;
    JLabel lblDni;
    JLabel lblTfnoChofer;
    JLabel lblDccionChofer;
    JLabel lblBuscarChofer;
    JLabel lblTitulo;
    JTextField txtNombreChofer;
    JTextField txtApellidosChofer;
    DatePicker fechaNac;
    DatePicker fechaInicio;
    DatePicker fechaFin;
    JTextField txtNSegSoc;
    JTextField txtDni;
    JTextField txtTfnoChofer;
    JTextField txtDccionChofer;
    JTextField txtBuscarChofer;
    JScrollPane scrollChoferes;
    JTable tablaChoferes;
    DefaultTableModel dtmChoferes;
    JButton btnAddChofer;
    JButton btnDelChofer;
    JButton btnModChofer;
    JButton btnListChofer;

    //MENU
    JMenu menuArchivo;
    JMenuItem itemSalir;
    JMenuItem itemOpciones;
    JMenuItem itemUsuarios;
    
    public VistaAdmin(){
        this.setSize(1024,860);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        inicioPanel();
    }

    private void inicioPanel() {
        panelChoferes= new JPanel();
        panelChoferes.setLayout(null);
        panelChoferes.setBackground(new Color(254, 242, 113));
        panelChoferes.setFont(new Font("Sans Serif", Font.BOLD, 24));
        this.getContentPane().add(panelChoferes);
        etiquetas();
        campos();
        botones();
        tablas();
        crearMenu();
        logoVentana();
    }

    private void logoVentana() {
        JLabel lblLogo = new JLabel();
        lblLogo.setBounds(450, 0, 508, 122);
        lblLogo.setIcon(new ImageIcon("img/logoventana.png"));
        panelChoferes.add(lblLogo);

        lblTitulo= new JLabel("Chóferes");
        lblTitulo.setBounds(300, 10, 200, 50);
        lblTitulo.setFont(new Font("Sans Serif", Font.BOLD, 30));
        panelChoferes.add(lblTitulo);
    }

    private void etiquetas() {
        lblNombreChofer = new JLabel("Nombre");
        lblNombreChofer.setBounds(25, 125, 74, 25);
        formatoEtiquetas(lblNombreChofer, panelChoferes);

        lblApellidosChofer = new JLabel("Apellidos");
        lblApellidosChofer.setBounds(500, 125, 100, 25);
        formatoEtiquetas(lblApellidosChofer, panelChoferes);

        lblFechaNac = new JLabel("Fecha de nac.");
        lblFechaNac.setBounds(25, 175, 200, 25);
        formatoEtiquetas(lblFechaNac, panelChoferes);

        lblFechaInicio = new JLabel("Fecha de inicio");
        lblFechaInicio.setBounds(25, 225, 200, 25);
        formatoEtiquetas(lblFechaInicio, panelChoferes);

        lblFechaFin = new JLabel("Fecha fin");
        lblFechaFin.setBounds(410, 225, 200, 25);
        formatoEtiquetas(lblFechaFin, panelChoferes);

        lblNSegSoc = new JLabel("Nº Seg. social");
        lblNSegSoc.setBounds(410, 175, 200, 25);
        formatoEtiquetas(lblNSegSoc, panelChoferes);

        lblDni = new JLabel("DNI/NIE");
        lblDni.setBounds(745, 175, 100, 25);
        formatoEtiquetas(lblDni, panelChoferes);

        lblTfnoChofer = new JLabel("Teléfono");
        lblTfnoChofer.setBounds(745, 225, 100, 25);
        formatoEtiquetas(lblTfnoChofer, panelChoferes);

        lblDccionChofer = new JLabel("Dirección");
        lblDccionChofer.setBounds(25, 275, 100, 25);
        formatoEtiquetas(lblDccionChofer, panelChoferes);

        lblBuscarChofer = new JLabel();
        lblBuscarChofer.setBounds(590, 264, 50, 50);
        lblBuscarChofer.setIcon(new ImageIcon("img/lupa.png"));
        panelChoferes.add(lblBuscarChofer);
    }

    private void campos(){
        txtNombreChofer = new JTextField();
        txtNombreChofer.setBounds(120, 125, 350, 25);
        formatoTextoSinCursorDerecha(txtNombreChofer, panelChoferes);

        txtApellidosChofer = new JTextField();
        txtApellidosChofer.setBounds(610, 125, 355, 25);
        formatoTextoSinCursorDerecha(txtApellidosChofer, panelChoferes);

        fechaNac = new DatePicker();
        fechaNac.setBounds(175, 175, 200, 25);
        fechaNac.setFont(new Font("Sans Serif", Font.PLAIN, 20));
        panelChoferes.add(fechaNac);

        fechaInicio = new DatePicker();
        fechaInicio.setBounds(175, 225, 200, 25);
        panelChoferes.add(fechaInicio);

        fechaFin = new DatePicker();
        fechaFin.setBounds(514, 225, 200, 25);
        panelChoferes.add(fechaFin);

        txtNSegSoc = new JTextField();
        txtNSegSoc.setBounds(565, 175, 150, 25);
        formatoTexto(txtNSegSoc, panelChoferes);

        txtDni = new JTextField();
        txtDni.setBounds(840, 175, 125, 25);
        formatoTexto(txtDni, panelChoferes);

        txtTfnoChofer = new JTextField();
        txtTfnoChofer.setBounds(850, 225, 115, 25);
        formatoTexto(txtTfnoChofer, panelChoferes);

        txtDccionChofer = new JTextField();
        txtDccionChofer.setBounds(140, 275, 425, 25);
        formatoTextoSinCursorDerecha(txtDccionChofer, panelChoferes);

        txtBuscarChofer = new JTextField();
        txtBuscarChofer.setBounds(635, 275, 330, 25);
        formatoTextoSinCursorDerecha(txtBuscarChofer, panelChoferes);

        scrollChoferes = new JScrollPane();
        scrollChoferes.setBounds(135, 340, 830, 375);
        panelChoferes.add(scrollChoferes);

        tablaChoferes = new JTable();
        scrollChoferes.setViewportView(tablaChoferes);
        tablaChoferes.setBackground(Color.WHITE);
    }

    private void formatoEtiquetas(JLabel label, JPanel panel) {
        label.setFont(new Font("Sans Serif", Font.BOLD, 20));
        panel.add(label);
    }

    private void formatoTexto(JTextField jTextField, JPanel panel) {
        jTextField.setBackground(Color.WHITE);
        jTextField.setFont(new Font("Sans Serif", Font.PLAIN, 20));
        jTextField.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        panel.add(jTextField);
    }

    private void formatoTextoSinCursorDerecha(JTextField jTextField, JPanel panel) {
        jTextField.setBackground(Color.WHITE);
        jTextField.setFont(new Font("Sans Serif", Font.PLAIN, 20));
        panel.add(jTextField);
    }

    private void botones() {
        btnAddChofer = new JButton();
        btnAddChofer.setBounds(20, 355, 100, 66);
        btnAddChofer.setIcon(new ImageIcon("img/botonguardar.png"));
        formatoBotones(btnAddChofer);

        btnModChofer = new JButton();
        btnModChofer.setBounds(20, 450, 100, 66);
        btnModChofer.setIcon(new ImageIcon("img/botonmodificar.png"));
        formatoBotones(btnModChofer);

        btnDelChofer = new JButton();
        btnDelChofer.setBounds(20, 545, 100, 66);
        btnDelChofer.setIcon(new ImageIcon("img/botonborrar.png"));
        formatoBotones(btnDelChofer);

        btnListChofer = new JButton();
        btnListChofer.setBounds(20, 640, 100, 66);
        btnListChofer.setIcon(new ImageIcon("img/botonlistar.png"));
        formatoBotones(btnListChofer);
    }

    private void formatoBotones(JButton button) {
        button.setOpaque(false);
        button.setContentAreaFilled(false);
        button.setBorderPainted(false);
        panelChoferes.add(button);
    }

    private void tablas(){
        dtmChoferes = new DefaultTableModel();
        tablaChoferes.setModel(dtmChoferes);
        Object[] cabeceras = {"Nombre", "Apellidos", "Fecha nac", "Seg.Soc.", "DNI", "Fecha inicio", "Fecha fin",
                "Tfno", "Dccion"};
        dtmChoferes.setColumnIdentifiers(cabeceras);
        TableRowSorter<TableModel> sorter=new TableRowSorter<TableModel>(dtmChoferes);
        tablaChoferes.setRowSorter(sorter);
    }

    private void crearMenu() {
        JMenuBar menuBar = new JMenuBar();
        menuArchivo = new JMenu("Archivo");
        menuArchivo.setFont(new Font("Sans Serif", Font.BOLD, 15));
        itemOpciones = new JMenuItem("Opciones");
        addItems(itemOpciones, "Opciones");
        itemSalir = new JMenuItem("Salir");
        addItems(itemSalir, "Salir");
        itemUsuarios = new JMenuItem("Cambio de usuario");
        addItems(itemUsuarios, "Cambio de usuario");

        menuBar.add(menuArchivo);
        menuBar.add(Box.createHorizontalGlue());
        this.setJMenuBar(menuBar);

    }

    private void addItems(JMenuItem item, String nombreItem) {
        item.setActionCommand(nombreItem);
        item.setFont(new Font("Sans Serif", Font.BOLD, 15));
        menuArchivo.add(item);
    }
}
